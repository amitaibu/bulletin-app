# Building a bulletin board using Haskell, scotty and friends

This started out as a [blog post](https://gilmi.me/blog/post/2020/12/05/scotty-bulletin-board) for scotty and friends
but I've played with it more since and added a database (persistent-sqlite),
user registration and authentication (including cookies and sessions),
markdown support and a slightly nicer looking css.

![Example image](/demo.png)

## Build

```sh
stack build
```

## Run with

```sh
CONN_STRING='file:/tmp/bullet.db' bulletin-app
```

where `/tmp/bullet.db` is the path to the database file.

## Static executable

To compile a static executable using docker, uncomment the relevant lines in the `stack.yaml` file,
and rebuild with `stack build`.


{-# language OverloadedStrings #-}

module Web.Bulletin.Html where

import qualified Data.Text.Lazy as TL
import qualified Lucid as H

import qualified Cheapskate.Lucid as MD
import qualified Cheapskate as MD (markdown, Options(..))


----------
-- HTML --
----------

type Html = H.Html ()

template :: TL.Text -> Html -> Html -> Html
template title header content =
  H.doctypehtml_ $ do
    H.head_ $ do
      H.meta_ [ H.charset_ "utf-8" ]
      H.title_ (H.toHtml title)
      H.link_ [ H.rel_ "stylesheet", H.type_ "text/css", H.href_ "/style.css"  ]
    H.body_ $ do
      H.div_ [ H.class_ "head" ] $ do
        H.h1_ [ H.class_ "logo" ] $
          H.a_ [H.href_ "/"] "Bulletin Board"
        header
      H.div_ [ H.class_ "main" ] $ do
        content

fromMarkdown :: TL.Text -> Html
fromMarkdown =
  MD.renderDoc . MD.markdown markdownOptions . TL.toStrict . TL.filter (/='\r')

markdownOptions :: MD.Options
markdownOptions =
  MD.Options
    { MD.sanitize = True
    , MD.allowRawHtml = False
    , MD.preserveHardBreaks = True
    , MD.debug = False
    }

{-# LANGUAGE OverloadedStrings #-}

module Web.Bulletin.DbAccess where

import qualified Data.Text.Lazy as TL
import qualified Database.Persist as P
import qualified Database.Persist.Sql as P
import qualified Database.Persist.Sqlite as PSqlite3

import Web.Bulletin.Config
import Web.Bulletin.Model
import qualified Web.Scotty.Sqlite.Users as Users


-- General --

type Sql a = P.SqlPersistT IO a

runDB :: Config -> P.SqlPersistT IO a -> IO a
runDB cfg dbop = P.runSqlPool dbop (cfgDbPool cfg)

dbMigrations :: Config -> IO ()
dbMigrations cfg = runDB cfg $ PSqlite3.runMigration migrateAll

-- Posts --

getAllPosts :: Config -> IO Posts
getAllPosts cfg = do
  runDB cfg $ PSqlite3.rawSql
    "select ??, ?? from login inner join post on login.id = post.author_id;"
    []

getPost :: PostId -> Sql (Maybe Post')
getPost pid = do
  mpost <- P.get pid
  maybe
    (pure Nothing)
    ( \post ->
      fmap ((,) (P.Entity pid post) . P.Entity (postAuthorId post))
        <$> P.get (postAuthorId post)
    )
    mpost

getAuthorPosts :: Config -> Users.LoginId -> IO Posts
getAuthorPosts cfg uid = do
  runDB cfg $ do
    muser <- P.get uid
    maybe
      (pure [])
      ( \author ->
        fmap (flip (,) (P.Entity uid author))
          <$> P.selectList [ PostAuthorId P.==. uid ] []
      )
      muser

insertPost :: Config -> Post -> IO (P.Key Post)
insertPost cfg = runDB cfg . P.insert

----

data EditPostStatus
  = Success
  | FailDoesn'tExist
  | FailNotAllowed

checkPost :: Users.LoginId -> PostId -> Sql EditPostStatus
checkPost uid pid = do
  mpost <- getPost pid
  case mpost of
    Just (P.Entity _ Post{postAuthorId=aid}, _)
      | uid == aid -> do
        pure Success
      | otherwise ->
        pure FailNotAllowed
    Nothing ->
      pure FailDoesn'tExist
  

editPost :: Config -> Users.LoginId -> PostId -> TL.Text -> TL.Text -> IO EditPostStatus
editPost cfg uid pid title content =
  runDB cfg $ do
    status <- checkPost uid pid
    case status of
      Success ->
        P.update pid
          [ PostTitle PSqlite3.=. title
          , PostContent PSqlite3.=. content
          ]
      _ ->
        pure ()
    pure status

deletePost :: Config -> Users.LoginId -> PostId -> IO EditPostStatus
deletePost cfg uid pid =
  runDB cfg $ do
    status <- checkPost uid pid
    case status of
      Success ->
        P.delete pid
      _ ->
        pure ()
    pure status


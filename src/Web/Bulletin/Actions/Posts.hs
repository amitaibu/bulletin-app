{-# language OverloadedStrings #-}

module Web.Bulletin.Actions.Posts where


import Data.Maybe (isJust)
import Control.Monad
import Control.Monad.IO.Class (liftIO)
import qualified Data.Text.Lazy as TL
import qualified Lucid as H
import qualified Web.Scotty as S
import qualified Database.Persist.Sql as P
import qualified Data.Time.Clock as C
import qualified Network.HTTP.Types as HTTP

import qualified Web.Scotty.Sqlite.Users as Users
import qualified Web.Scotty.Sqlite.Users.Login as Login
import qualified Web.Bulletin.DbAccess as DB
import Web.Bulletin.Config
import Web.Bulletin.Html
import Web.Bulletin.Model
import Web.Bulletin.Validation

-------------
-- Routing --
-------------

router :: Config -> Users.SessionStore -> S.ScottyM ()
router cfg sessionStore = do
  -- A page for a specific post
  S.get "/post/:id" $ do
    pid <- P.toSqlKey <$> S.param "id"
    displayPost cfg sessionStore pid

  -- A page for creating a new post
  S.get "/new/post" $
    Users.withLogin (cfgDbPool cfg) sessionStore $ \_ ->
      servePostForm cfg sessionStore New noNewPostErrors "" ""

  -- A request to submit a new page
  S.post "/new/post" $
    Users.withLogin (cfgDbPool cfg) sessionStore $ \user -> do
      title <- S.param "title"
      content <- S.param "content"
      submitNewPostForm cfg sessionStore title content (Users.getLoginId user)

  -- serve edit post form
  S.get "/post/:id/edit" $
    Users.withLogin (cfgDbPool cfg) sessionStore $ \_ -> do
      pid <- P.toSqlKey <$> S.param "id"
      mpost <- liftIO $ DB.runDB cfg $ DB.getPost pid
      case mpost of
        Nothing -> do
          S.status HTTP.notFound404
          S.text "404 Not Found."
        Just (P.Entity _ post, _) ->
          servePostForm cfg sessionStore (Edit pid) noNewPostErrors (postTitle post) (postContent post)

  -- A request to submit a editing of a page
  S.post "/post/:id/edit" $
    Users.withLogin (cfgDbPool cfg) sessionStore $ \user -> do
      pid <- P.toSqlKey <$> S.param "id"
      title <- S.param "title"
      content <- S.param "content"
      submitEditPostForm cfg sessionStore (Users.getLoginId user) pid title content

  -- A request to delete a specific post
  S.post "/post/:id/delete" $
    Users.withLogin (cfgDbPool cfg) sessionStore $ \user -> do
      pid <- P.toSqlKey <$> S.param "id"
      deletePost cfg (Users.getLoginId user) pid

data Route
  = New
  | Edit PostId

-------------
-- Actions --
-------------

displayAllPosts :: Config -> Users.SessionStore -> S.ActionM Html
displayAllPosts cfg sessionStore = do
  mses <- Users.getSession sessionStore
  posts <- liftIO $ DB.getAllPosts cfg
  pure $ do
    when (isJust mses) $ do
      H.p_ [ H.class_ "new-button" ] $ H.a_ [H.href_ "/new/post"] "New Post"
    H.div_ [ H.class_ "posts" ] $
      mapM_ (postHtml False mses) $ reverse posts


displayPost :: Config -> Users.SessionStore -> PostId -> S.ActionM ()
displayPost cfg sessionStore pid = do
  mpost <- liftIO $ DB.runDB cfg $ DB.getPost pid
  mses <- Users.getSession sessionStore
  io <- Login.loginOrLogout (cfgDbPool cfg) sessionStore
  case mpost of
    Just post ->
      S.html $
        H.renderText $
          template
            ("Bulletin board - post " <> TL.pack (show pid))
            io
            (postHtml True mses post)

    Nothing -> do
      S.status HTTP.notFound404
      S.html $
        H.renderText $
          template
            ("Bulletin board - post " <> TL.pack (show pid) <> " not found.")
            ""
            "404 Post not found."

servePostForm :: Config -> Users.SessionStore -> Route -> NewPostErrors -> TL.Text -> TL.Text -> S.ActionM ()
servePostForm cfg sessionStore route errs title content = do
  io <- Login.loginOrLogout (cfgDbPool cfg) sessionStore
  S.html $
    H.renderText $
      template
        ("Bulletin board - add new post")
        io
        (newPostHtml route errs title content)

submitNewPostForm :: Config -> Users.SessionStore -> TL.Text -> TL.Text -> Users.LoginId -> S.ActionM ()
submitNewPostForm cfg ses title content uid = do
  let
    errs = validateNewPost title content
  if hasNewPostErrors errs
    then do
      servePostForm cfg ses New errs title content
    else do
      time <- liftIO C.getCurrentTime
      pid <- liftIO $ DB.insertPost cfg
        ( Post
          { postDate = time
          , postAuthorId = uid
          , postTitle = title
          , postContent = content
          }
        )
      S.redirect ("/post/" <> showPostKey pid)

submitEditPostForm :: Config -> Users.SessionStore -> Users.LoginId -> PostId -> TL.Text -> TL.Text -> S.ActionM ()
submitEditPostForm cfg ses uid pid title content = do
  let
    errs = validateNewPost title content
  if hasNewPostErrors errs
    then do
      servePostForm cfg ses (Edit pid) errs title content
    else do
      status <- liftIO $ DB.editPost cfg uid pid title content
      case status of
        DB.Success ->
          S.redirect ("/post/" <> showPostKey pid)
        DB.FailNotAllowed -> do
          S.status HTTP.forbidden403
          S.text "You do not have permissions to edit this post."
        DB.FailDoesn'tExist -> do
          S.status HTTP.notFound404
          S.text "404 Not Found."

deletePost :: Config -> Users.LoginId -> PostId -> S.ActionM ()
deletePost cfg uid pid = do
  status <- liftIO $ DB.deletePost cfg uid pid
  case status of
    DB.Success ->
      S.redirect "/"
    DB.FailNotAllowed -> do
      S.status HTTP.forbidden403
      S.text "You do not have permissions to edit this post."
    DB.FailDoesn'tExist -> do
      S.status HTTP.notFound404
      S.text "404 Not Found."

----------
-- Html --
----------

postHtml :: Bool -> Maybe Users.LoginId -> Post' -> Html
postHtml displayEdit muid (P.Entity pidKey post, P.Entity _ user) = do
  H.div_ [ H.class_ "post" ] $ do
    when (displayEdit && muid == Just (postAuthorId post)) $ do
      H.section_ [ H.class_ "edit-post-section" ] $ do
        H.form_
          [ H.method_ "get"
          , H.action_ (TL.toStrict $ "/post/" <> showPostKey pidKey <> "/edit")
          , H.class_ "edit-post"
          ]
          ( do
            H.input_ [H.type_ "submit", H.value_ "E", H.class_ "editbtn", H.title_ "Edit"]
          )
        H.form_
          [ H.method_ "post"
          , H.action_ (TL.toStrict $ "/post/" <> showPostKey pidKey <> "/delete")
          , H.onsubmit_ "return confirm('Are you sure?')"
          , H.class_ "delete-post"
          ]
          ( do
            H.input_ [H.type_ "submit", H.value_ "X", H.class_ "deletebtn", H.title_ "Delete"]
          )

    H.div_ [ H.class_ "post-header" ] $ do
      H.h2_ [ H.class_ "post-title" ] $
        H.a_
          [H.href_ (TL.toStrict $ "/post/" <> showPostKey pidKey)]
          (H.toHtml $ postTitle post)

      H.span_ $ do
        H.p_ [ H.class_ "post-time" ] $ H.toHtml (TL.pack (show (postDate post)))
        H.p_ [ H.class_ "post-author" ] $ "by " <> H.toHtml (Users.loginDisplayName user)

    H.div_ [H.class_ "post-content"] $ do
      fromMarkdown $ postContent post

newPostHtml :: Route -> NewPostErrors -> TL.Text -> TL.Text -> Html
newPostHtml route errs title content = do
  H.form_
    [ H.method_ "post"
    , H.action_ $ case route of
      New -> "/new/post"
      Edit pid -> TL.toStrict $ "/post/" <> showPostKey pid <> "/edit"
    , H.class_ "new-post"
    ]
    ( do
      applyMaybe (npeTitle errs) (H.p_ [ H.class_ "error" ] . H.toHtml)
      H.p_ $ H.input_ $
        [ H.type_ "text"
        , H.name_ "title"
        , H.required_ "true"
        , H.placeholder_ "Title..."
        , H.value_ (TL.toStrict title)
        ] <> newPostFocus Title errs

      applyMaybe (npeContent errs) (H.p_ [ H.class_ "error" ] . H.toHtml)
      H.p_ $ H.textarea_
        ([H.name_ "content", H.required_ "true", H.placeholder_ "Content..."] <> newPostFocus Content errs)
        (H.toHtml content)

      let
        btntext = case route of
          New -> "Create"
          Edit{} -> "Update"
      H.p_ $ H.input_ [H.type_ "submit", H.value_ btntext, H.class_ "submit-button"]
    )


newPostFocus :: ErrorPart -> NewPostErrors -> [H.Attribute]
newPostFocus part errs =
  case part of
    Title
      | not (hasNewPostErrors errs) || isJust (npeTitle errs) ->
        [H.autofocus_]
    Content
      | not (isJust (npeTitle errs)) && isJust (npeContent errs) ->
        [H.autofocus_]
    _ -> []


----------------
-- Validation --
----------------

data ErrorPart
  = Title
  | Content

data NewPostErrors
  = NewPostErrors
    { npeTitle :: Maybe TL.Text
    , npeContent :: Maybe TL.Text
    }
    deriving (Show, Eq)

noNewPostErrors :: NewPostErrors
noNewPostErrors = NewPostErrors Nothing Nothing

hasNewPostErrors :: NewPostErrors -> Bool
hasNewPostErrors errs =
  errs /= noNewPostErrors

validateNewPost :: TL.Text -> TL.Text -> NewPostErrors
validateNewPost title content =
  NewPostErrors
    { npeTitle =
      if TL.length title < 1 || TL.length title > 200
        then
          Just $ TL.unwords
            [ "Title must be at least 1 character long and at most 200 characters long."
            , "Current length is: " <> TL.pack (show (TL.length title))
            ]
        else
          Nothing
    , npeContent =
      if TL.length content < 10 || 2000 < TL.length content
        then
          Just $ TL.unwords
            [ "Content must be at least 10 characters long and at most 2000 characters long."
            , "Current length is: " <> TL.pack (show (TL.length content))
            ]
        else
          Nothing
    }

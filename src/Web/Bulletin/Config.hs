{-# language TypeApplications #-}

module Web.Bulletin.Config where

import qualified Data.Text as T
import qualified Control.Monad.Logger as Log
import qualified Database.Persist.Sqlite as PSqlite3
import System.Environment (lookupEnv)
import GHC.Conc (numCapabilities)

data Config
  = Config
    { cfgEnv :: Environment
    , cfgDbPool :: PSqlite3.ConnectionPool
    , cfgPort :: Port
    }

getConfig :: IO Config
getConfig = do
  env <- getEnv
  pool <- getPool env
  port <- getPort env
  pure $ Config env pool port

data Environment
  = Development
  | Production
  | Testing
  deriving (Eq, Read, Show, Enum, Bounded)

getEnv :: IO Environment
getEnv =
  maybe Development readEnv <$> lookupEnv "SCOTTY_ENV"

readEnv :: String -> Environment
readEnv str =
  case reads str of
    [(env, "")] -> env
    _ -> error $ unlines $
      [ "Could not parse: '" <> str <> "' as a valid environment options."
      , "Expecting one of the following:"
      ] <>
      map (("- " <>) . show @Environment) [ minBound .. maxBound ]

getPool :: Environment -> IO PSqlite3.ConnectionPool
getPool env = do
  cs <- getConnectionString env
  let
    poolSize = ceiling (fromIntegral numCapabilities / 2)
  case env of
    Development -> Log.runStderrLoggingT
      (PSqlite3.createSqlitePool cs poolSize)
    Production -> Log.runStderrLoggingT
      (PSqlite3.createSqlitePool cs poolSize)
    Testing -> Log.runNoLoggingT
      (PSqlite3.createSqlitePool cs poolSize)

type Port = Int

getPort :: Environment -> IO Int
getPort env = do
  m <- lookupEnv "PORT"
  case m of
    Just mp ->
      case reads mp of
        [(port, [])] -> pure port
        _ -> error "Failed to read PORT environment option"
    Nothing ->
      pure $ case env of
        Development -> 8080
        Production -> 80
        Testing -> 8000

type ConnectionString = T.Text

getConnectionString :: Environment -> IO ConnectionString
getConnectionString e =
  case e of
--    Development ->
--      pure $ T.pack ":memory:"
--    Test ->
--      pure $ T.pack ":memory:"
--    Production ->
    _ ->
      maybe (error "could not find parameter 'CONN_STRING'") T.pack <$> lookupEnv "CONN_STRING"

{-# language OverloadedStrings #-}

module Web.Bulletin.Router where

import qualified Web.Scotty as S
import qualified Lucid as H

import Web.Bulletin.Config
import Web.Bulletin.Style
import Web.Bulletin.Html

import qualified Web.Scotty.Sqlite.Users as Users
import qualified Web.Bulletin.Actions.Posts as Posts

-------------
-- Routing --
-------------

router :: Config -> Users.SessionStore -> S.ScottyM ()
router cfg sessionStore = do
  -- Our main page, which will display all of the bulletins
  S.get "/" $ index cfg sessionStore

  Users.router (cfgDbPool cfg) sessionStore mytemplate

  Posts.router cfg sessionStore


  -- css styling
  S.get "/style.css" $ do
    S.setHeader "Content-Type" "text/css; charset=utf-8"
    S.raw style

mytemplate :: Users.HtmlTemplate
mytemplate title = template ("Bulletin Board - " <> title) ""

index :: Config -> Users.SessionStore -> S.ActionM ()
index cfg sessionStore = do
  posts <- Posts.displayAllPosts cfg sessionStore
  io <- Users.loginOrLogout (cfgDbPool cfg) sessionStore
  S.html $
    H.renderText $
      template
        ("Bulletin board")
        io
        posts

{-# language OverloadedStrings #-}

module Web.Bulletin.Run where

import qualified Network.Wai as Wai
import qualified Network.Wai.Middleware.RequestLogger as WaiLog
import qualified Web.Scotty as S
import qualified Data.Text.Lazy as TL
import qualified Network.HTTP.Types as HTTP
import Control.Monad.IO.Class (liftIO)
import qualified Lucid as H

import qualified Web.Scotty.Sqlite.Users as Users
import qualified Web.Bulletin.DbAccess as DB
import Web.Bulletin.Config
import Web.Bulletin.Html
import Web.Bulletin.Router

------------
-- Runner --
------------

run :: IO ()
run = do
  cfg <- getConfig
  liftIO $ Users.dbMigrations (cfgDbPool cfg)
  liftIO $ DB.dbMigrations cfg
  ses <- Users.initSessionStore
  S.scotty (cfgPort cfg) (myApp cfg ses)

myApp :: Config -> Users.SessionStore -> S.ScottyM ()
myApp cfg ses = do
  S.middleware (loggingM (cfgEnv cfg))
  S.defaultHandler (defaultH (cfgEnv cfg))
  router cfg ses

-----------------
-- Middlewares --
-----------------

loggingM :: Environment -> Wai.Middleware
loggingM e = case e of
  Development -> WaiLog.logStdoutDev
  Production -> WaiLog.logStdout
  Testing -> id

defaultH :: Environment -> TL.Text -> S.ActionM ()
defaultH env err = do
  S.status HTTP.internalServerError500
  S.html $
    H.renderText $
      template
        ("Internal Server Error 500")
        ""
        $ case env of
          Development ->
            H.toHtml err
          Testing ->
            H.toHtml err
          Production ->
            ""

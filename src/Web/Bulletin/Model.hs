{-# language TemplateHaskell #-}
{-# language QuasiQuotes #-}
{-# language TypeFamilies #-}
{-# language GADTs #-}
{-# LANGUAGE DerivingStrategies #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE StandaloneDeriving #-}
{-# LANGUAGE UndecidableInstances #-}
{-# LANGUAGE MultiParamTypeClasses #-}

module Web.Bulletin.Model where

import qualified Data.Time.Clock as C
import qualified Data.Text.Lazy as TL
import qualified Database.Persist as P
import qualified Database.Persist.Sql as P
import Database.Persist.TH

import qualified Web.Scotty.Sqlite.Users.Model as Users

share [mkPersist sqlSettings, mkMigrate "migrateAll"] [persistLowerCase|
Post
    title TL.Text
    content TL.Text
    date C.UTCTime
    authorId Users.LoginId
    deriving Show

Comment
    content TL.Text
    date C.UTCTime
    postId PostId
    authorId Users.LoginId
    deriving Show
|]

type Post' = (P.Entity Post, P.Entity Users.Login)
type Posts = [Post']
type Logins = [P.Entity Users.Login]
-- type Comments = [P.Entity Comment]


showPostKey :: PostId -> TL.Text
showPostKey pk =
  case head $ P.keyToValues pk of
    P.PersistInt64 i -> TL.pack (show i)
    v -> error $ "Unexpected result for keyToValues in showPostKey: " <> show v
